package pl.sda.poznan.math;

/**
 * Klasa pomocnicza do obliczen matematycznych
 *
 * @author siwipi
 * @since 1.0
 */
public class MathHelper {

  /**
   * Pomocnicza klasa, wiec zabraniamy tworzenia obiektow
   */
  private MathHelper() {
    throw new UnsupportedOperationException("Helper class");
  }

  /**
   * Metoda obliczająca n!
   *
   * @param n - liczba z ktorej chcesz policzyc silnie
   * @return zwracany wynik
   */
  public static int factorial(int n) {
    if (n < 0) {
      throw new IllegalArgumentException("Liczba musi byc wieksza od 0");
    }
    if (n == 0 || n == 1) {
      return 1;
    }
    return n * factorial(n - 1);
  }

}
