package pl.sda.poznan;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import pl.sda.poznan.math.MathHelper;

public class Program {

  public static void main(String[] args) {
    System.out.println("Podaj liczbe wieksza od zera: ");
    Scanner sc = new Scanner(System.in);
    int number = sc.nextInt();
    System.out.println(number);
    try{
      int result = MathHelper.factorial(number);
      System.out.println(String.format("Dla %s! wynik to: %d", number, result));
    }catch (IllegalArgumentException ex){
      System.out.println(ex.getMessage());
    }

    Integer integer = Integer.valueOf("5");


    List<String> lst = new ArrayList<>();
    new LinkedList<String>();
  }
}
