package pl.sda.poznan.playground;

import java.util.ArrayList;
import java.util.List;

public class Application {

  public static void foo(int param) {
    System.out.println(" W funkcji foo" + param); // 2
    param = 100;
    System.out.println("W funkcji foo, po zmianie: " + param); // 3
  }

  public static void bar(Person param) {
    System.out.println(param.getAge()); // 50
    param.setAge(100);
    System.out.println(param.getAge()); // 100
    param = null;
  }

  public static void main(String[] args) {

    List<Integer> lst = new ArrayList<>();

    Integer price = null;

//    int a = 200;
//    System.out.println(a); // 1
//    foo(a);
//    System.out.println(a); // 4

    // object types:
    Person p = new Person();
    p.setAge(50);
    System.out.println(p.getAge()); // 50
    bar(p);
    System.out.println(p.getAge()); // 100

  }

  // 1 - 200
  // 2 - 200
  // 3 - 100
  // 4- 200 vs 100?
}
